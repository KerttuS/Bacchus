﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using Bacchus.Models;

namespace Bacchus.Controllers
{
    public class ProductsController : Controller
    {
        private BacchusContext db = new BacchusContext();

        // GET: Products
        public ActionResult Index(string searchCategory, int? productsId)
        {
            List<Products> products = new List<Products>();

            HttpClient client = new HttpClient();
            var url = "http://uptime-auction-api.azurewebsites.net/api/Auction";
            var response = client.GetAsync(url).Result;

            var productIds = db.Products.Select(x => x.ProductId).ToList();

            if (response.IsSuccessStatusCode)
            {
                products = response.Content.ReadAsAsync <List<Products>>().Result;
                var productsToAdd = products.Where(x => !productIds.Contains(x.ProductId)).ToList();

                db.Products.AddRange(productsToAdd);
                db.SaveChanges();
            }

            ViewBag.searchCategory = new SelectList(products.Select(x => x.ProductCategory).Distinct().ToList(), String.Empty);
        
            if(!string.IsNullOrEmpty(searchCategory))
            {
                
                products = products.Where(s => s.ProductCategory.Contains(searchCategory)).ToList();
                
                return View(products.Where(p => p.BiddingEndDate > DateTime.Now.Subtract(TimeSpan.FromDays(1))));
                
            }
            
            return View(db.Products.ToList().Where(x => x.BiddingEndDate > DateTime.Now.Subtract(TimeSpan.FromDays(1))));
            
        }
        


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
