﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Bacchus.Models;

namespace Bacchus.Controllers
{
    public class OffersController : Controller
    {
        private BacchusContext db = new BacchusContext();

        // GET: Offers
        public ActionResult Index()
        {

            var offers = db.Offers.Include(s => s.Products); 
            
            return View(offers);
        }

       
        // GET: Offers/Create
        public ActionResult Create(int? productsid)
        {
            
            if (productsid == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            return View();

        }

        // POST: Products2/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,ProductsId,FirstName,LastName,Amount,OfferDate")] Offers offers)
        {

            if (ModelState.IsValid)

            {
                offers.OfferDate = DateTime.Now;
                db.Offers.Add(offers);
                db.SaveChanges();

                TempData["Salvestatud"] = "Sinu pakkumine on salvestatud";

                return View();
            }
            

            return RedirectToAction("Index", "Products");
        }

       
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
