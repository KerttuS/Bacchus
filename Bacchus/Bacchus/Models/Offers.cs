﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Bacchus.Models
{
    public class Offers
    {

        public int Id { get; set; }
        [Display(Name = "Summa (EUR)")]
        [Required(ErrorMessage = "Summa on nõutav ja peab olema number")]
        [Range(1, 1000000, ErrorMessage = "Summa peab olema vahemikus 1 - 1000000.")]      
        public double Amount { get; set; }
        [Display(Name = "Eesnimi")]
        [Required(ErrorMessage = "Nimi on nõutav")]
        public string FirstName { get; set; }
        [Display(Name = "Perekonnanimi")]
        [Required(ErrorMessage = "Nimi on nõutav")]
        public string LastName { get; set; }
        [DataType(DataType.DateTime)]
        public DateTime? OfferDate { get; set; }
        public int ProductsId { get; set; }
        public Products Products { get; set; }


        [NotMapped]
        public string Name
        {
            get
            {
                if (FirstName != null && LastName != null)
                {
                    return String.Format("{0}{1}", FirstName, LastName);
                }
                else
                {
                    return String.Format("");
                }
            }


        }
    }
}