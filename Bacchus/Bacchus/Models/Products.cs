﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Bacchus.Models
{
    public class Products
    {
        public int Id { get; set; }
        public string ProductId { get; set; }
        [Display (Name = "Toode")]
        public string ProductName { get; set; }
        [Display(Name = "Kirjeldus")]
        public string ProductDescription { get; set; }
        public string ProductCategory { get; set; }
        public IEnumerable<SelectListItem> Category { get; set; }
        [Display(Name = "Pakkumise lõpp")]
        public DateTime? BiddingEndDate { get; set; }
        public ICollection<Offers> Offers { get; set; }
    }
}